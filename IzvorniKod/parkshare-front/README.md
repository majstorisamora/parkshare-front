## Koraci za pokrenuti projekt lokalno i povezati se na [Parkshare back](https://gitlab.com/majstorisamora/parkshare-back)
1. U mapi `/IzvorniKod/parkshare-front/` pokrenuti `npm install` koji instalira sve potrebne module za projekt.
2. Napraviti `.env.local` datoteku u repozitoriju `/IzvorniKod/parkshare-front/` i u nju dodati ovu liniju koda `REACT_APP_BACKEND_URL=http://localhost:8080`
3. Pokrenuti projekt s `npm start` i front će se automatski otvoriti na [localhost:3000](http://localhost:3000) i updateati svakom promjenom koda :)


# **https://parkshare-front.netlify.app/**
