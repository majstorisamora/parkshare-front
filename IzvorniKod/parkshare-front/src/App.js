import "./App.less";
import { Redirect, Route, Switch } from "react-router-dom";
import { LoginScreenContainer } from "./screens/Login/LoginScreenContainer";
import { LayoutContainer } from "./components/LayoutContainer/LayoutContainer";
import { PrivateRoute } from "./routing/PrivateRoute";
import { SignUpContainer } from "./screens/Signup/SignUpContainer";
import { AdminRouter } from "./routing/AdminRouter";
import { HomeRouter } from "./routing/HomeRouter";
import { LandingScreen } from "./screens/Landing/LandingScreen";
import { Screen404 } from "./screens/404/Screen404";
import { ParkingOwnerRouter } from "./routing/ParkingOwnerRouter";
import { ParkingListAndMapContainer } from "./screens/Home/Parking/ParkingListAndMapContainer";

function App() {
  return (
    <div>
      <Switch>
        <PrivateRoute path={"/admin/*"}>
          <LayoutContainer>
            <AdminRouter />
          </LayoutContainer>
        </PrivateRoute>
        <PrivateRoute path={"/parkingOwner/*"}>
          <LayoutContainer>
            <ParkingOwnerRouter />
          </LayoutContainer>
        </PrivateRoute>
        <PrivateRoute path={"/home/*"}>
          <LayoutContainer>
            <HomeRouter />
          </LayoutContainer>
        </PrivateRoute>
        <Route exact path={"/parking/all"}>
          <LayoutContainer>
            <ParkingListAndMapContainer />
          </LayoutContainer>
        </Route>
        <Route exact path={"/signup"} component={SignUpContainer} />
        <Route exact path={"/login"} component={LoginScreenContainer} />
        <Route exact path={"/"} component={LandingScreen} />
        <Redirect from={"/register"} to={"/signup"} />
        <Route path={"*"} component={Screen404} />
      </Switch>
    </div>
  );
}

export default App;
