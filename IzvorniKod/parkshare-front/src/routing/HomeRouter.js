import { Route } from "react-router-dom";
import { ParkingListAndMapContainer } from "../screens/Home/Parking/ParkingListAndMapContainer";
import { ParkingSpotsForParking } from "../screens/Home/ParkingSpot/ParkingSpotsForParking";
import { UserSettingsContainer } from "../screens/UserSettings/UserSettingContainer";
import { DashboardContainer } from "../screens/Home/Dashboard/DashboardContainer";
import { useAuth } from "../hooks/useAuth";
import { PODashboardContainer } from "../screens/Home/PODashboard/PODashboardContainer";
import { ParticularDayContainer } from "../screens/Home/ParticularDay/ParticularDayContainer";
import { RouteToNearest } from "../screens/Home/RouteToNearest/RouteToNearest";

export const HomeRouter = () => {
  const { user } = useAuth();
  return (
    <>
      <Route exact path={"/home/parking/all"}>
        <ParkingListAndMapContainer />
      </Route>
      <Route exact path={"/home/parkingSpots/:id"}>
        <ParkingSpotsForParking />
      </Route>
      <Route exact path={"/home/settings"}>
        <UserSettingsContainer />
      </Route>
      <Route exact path={"/home/particularDayReservation"}>
        <ParticularDayContainer />
      </Route>
      <Route exact path={"/home/dashboard"}>
        {user?.userRole === "CLIENT" ? (
          <DashboardContainer />
        ) : (
          <PODashboardContainer />
        )}
      </Route>
      <Route exact path={"/home/routeToNearest"}>
        <RouteToNearest />
      </Route>
    </>
  );
};
