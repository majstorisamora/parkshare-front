import { Route } from "react-router-dom";
import { ParkingContainer } from "../screens/ParkingOwner/Parking/ParkingContainer";
import { ParkingSpotsContainer } from "../screens/ParkingOwner/ParkingSpots/ParkingSpotsContainer";
import { ParkingSpotsTable } from "../screens/ParkingOwner/ParkingSpotsTable/ParkingSpotsTable";

export const ParkingOwnerRouter = () => {
  return (
    <>
      <Route exact path={"/parkingOwner/myParking"}>
        <ParkingContainer />
      </Route>
      <Route exact path={"/parkingOwner/myParkingSpots"}>
        <ParkingSpotsTable />
      </Route>
      <Route exact path={"/parkingOwner/addParkingSpot"}>
        <ParkingSpotsContainer />
      </Route>
    </>
  );
};
