import { Route, useHistory } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";

export const PrivateRoute = ({ path, children }) => {
  const history = useHistory();
  const { user } = useAuth();

  const ele =
    user || localStorage.parkshareState ? children : history.push("/login");

  const check = {
    if(user) {
      user.userRole !== "ADMIN" &&
        path.includes("/admin") &&
        history.push("/home");
      user.userRole !== "PARKINGOWNER" &&
        path.includes("/parkingOwner") &&
        history.push("/home");
    },
  };

  return <Route path={path}>{ele}</Route>;
};
