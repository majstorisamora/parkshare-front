import { Route } from "react-router-dom";
import { AdminUsersContainer } from "../screens/Admin/Users/AdminUsersContainer";
import { AdminParkingOwnersContainer } from "../screens/Admin/ParkingOwners/AdminParkingOwnersContainer";

export const AdminRouter = () => {
  return (
    <>
      <Route exact path={"/admin/users"}>
        <AdminUsersContainer />
      </Route>
      <Route exact path={"/admin/activateOwners"}>
        <AdminParkingOwnersContainer />
      </Route>
    </>
  );
};
