import { getByTestId, render } from "@testing-library/react";
import { SignUpForm } from "./SignUpForm";

describe("Testing Login Form Render", () => {
  window.matchMedia =
    window.matchMedia ||
    function () {
      return {
        matches: false,
        addListener: function () {},
        removeListener: function () {},
      };
    };

  it("check for elements in form are rendering correctly", () => {
    const { getByTestId } = render(<SignUpForm />);

    // Get elements to be checked if they render correctly
    const form = getByTestId("formSignUpTest");
    const usernameInput = getByTestId("formInputUsername");
    const usernamePassword = getByTestId("formInputPassword");

    // Check elements to be true
    expect(form).toBeTruthy();
    expect(usernameInput).toBeTruthy();
    expect(usernamePassword).toBeTruthy();
  });
});
