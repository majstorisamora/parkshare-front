import { Button, Checkbox, Divider, Form, Input } from "antd";
import styles from "./SignUp.module.scss";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

export const SignUpForm = (props) => {
  const history = useHistory();
  const [formRef] = Form.useForm();
  const [picture, setPicture] = useState();

  const handleOnFinish = (values) => {
    props.handleSignUp(values, picture);
  };

  useEffect(() => {
    formRef.resetFields();
  }, [props.editUser]);

  return (
    <Form
      form={formRef}
      layout={"horizontal"}
      className={props.showDivider ? styles.formSignUp : styles.form}
      name="basic"
      initialValues={props.editUser}
      onFinish={handleOnFinish}
      data-testid="formSignUpTest"
    >
      {props.showDivider && <Divider>General</Divider>}
      <Form.Item
        label="E-mail"
        name="email"
        validateFirst={true}
        rules={[
          {
            required: true,
            message: "Please input your email address!",
          },
          {
            validator: (rule, value) => {
              return value.includes("@")
                ? Promise.resolve()
                : Promise.reject(new Error("Invalid email."));
            },
          },
        ]}
      >
        <Input data-testid="formInputUsername" />
      </Form.Item>
      <Form.Item
        label="Username"
        name="username"
        validateFirst={true}
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
          {
            validator: (rule, value) => {
              if (value.toLowerCase() === "admin") {
                return Promise.reject(new Error("Invalid username."));
              }
              return Promise.resolve();
            },
          },
        ]}
      >
        <Input.Password data-testid="formInputPassword" />
      </Form.Item>
      {props.showDivider && (
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
      )}
      <Form.Item
        label="IBAN"
        name="iban"
        validateFirst={true}
        rules={[
          {
            required: true,
            message: "Please input your IBAN!",
          },
          {
            message: "Invalid IBAN!",
            len: 21,
          },
        ]}
      >
        <Input />
      </Form.Item>
      {props.showDivider && <Divider>Profile</Divider>}
      <Form.Item
        rules={[
          {
            required: true,
            message: "Please upload profile picture.",
          },
        ]}
        label="Profile pic"
        name="imageUrl"
      >
        <input
          type={"file"}
          onChange={(d) => {
            setPicture(d.target.files[0]);
          }}
        />
      </Form.Item>
      <Form.Item
        label="First Name"
        name="firstName"
        rules={[
          {
            required: true,
            message: "Please input your first name!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Last Name"
        name="lastName"
        rules={[
          {
            required: true,
            message: "Please input your last name!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      {props.showDivider && (
        <Form.Item label="Role" name="isOwner" valuePropName="checked">
          <Checkbox>Parking owner</Checkbox>
        </Form.Item>
      )}
      <div
        className={
          props.showDivider ? styles.submitButtonSignUp : styles.submitButton
        }
      >
        {props.showDivider && (
          <Form.Item>
            <Button type={"link"} onClick={() => history.goBack()}>
              Back
            </Button>
          </Form.Item>
        )}

        <Form.Item>
          <Button loading={props.loading} type={"primary"} htmlType={"submit"}>
            {props.showDivider ? "Submit" : "Apply Changes"}
          </Button>
        </Form.Item>
      </div>
    </Form>
  );
};
