import { SignUpForm } from "./SignUpForm";
import { Layout, notification } from "antd";
import { Header } from "../../components/Header/Header";
import styles from "./SignUp.module.scss";
import { useHistory } from "react-router-dom";
import { useState } from "react";

export const SignUpContainer = () => {
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleSignUp = async (values, image) => {
    setLoading(true);
    const data = new FormData();
    data.append("username", values.username);
    data.append("firstName", values.firstName);
    data.append("lastName", values.lastName);
    data.append("iban", values.iban);
    data.append("password", values.password);
    data.append("email", values.email);
    data.append("userRole", values.isOwner ? "PARKINGOWNER" : "CLIENT");
    data.append("file", image);

    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/register`, {
      method: "POST",
      body: data,
    }).catch((err) => console.log(err));

    setLoading(false);
    res?.status === 200
      ? handleSuccess()
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          )
          .catch(() =>
            notification.error({ description: "Upali server dora." })
          );
  };

  const handleSuccess = () => {
    history.push("/");
    notification.success({
      description: "Successfully applied. Complete registration via email.",
      placement: "bottomRight",
    });
  };

  return (
    <div className={styles.layout}>
      <Layout>
        <Header />
        <SignUpForm
          loading={loading}
          showDivider={true}
          handleSignUp={handleSignUp}
        />
      </Layout>
    </div>
  );
};
