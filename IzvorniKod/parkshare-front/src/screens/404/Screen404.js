import { Button, Layout } from "antd";
import { useHistory } from "react-router-dom";
import styles from "./Screen404.module.scss";

const { Content, Footer } = Layout;

export const Screen404 = () => {
  const history = useHistory();

  return (
    <Layout style={{ height: "100vh" }}>
      <Content className={styles.content}>
        <h1>404: ParkShare page does not exist.</h1>
        <section>
          <Button type={"link"} onClick={() => history.push("/")}>
            Home
          </Button>
        </section>
      </Content>
      <Footer className={styles.footer}>
        Made with ❤️ &nbsp;by MajstoriSaMora
      </Footer>
    </Layout>
  );
};
