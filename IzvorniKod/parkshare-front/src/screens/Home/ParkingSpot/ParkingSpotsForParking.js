import { useEffect, useState } from "react";
import { Button, List, Tag } from "antd";
import { Map } from "../../../components/Map/Map";
import { useHistory, useParams } from "react-router-dom";
import { backend } from "../../../utils/api";
import styles from "./ParkingSpotsForParking.module.scss";
import { MapCoordinatesUtils } from "../../../utils/MapCoordinatesUtils";
import { DateAndTimeForm } from "../../../components/DateAndTimeForm/DateAndTimeForm";
import { ReservationModalConfirmation } from "./components/ReservationModalConfirmation";
import { ReservationUtils } from "../../../utils/ReservationUtils";
import { ParticularDayFilterModal } from "./components/ParticularDayFilterModal";
import { useAuth } from "../../../hooks/useAuth";

export const ParkingSpotsForParking = () => {
  const { id } = useParams();
  const { user } = useAuth();
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [parkingSpots, setParkingSpots] = useState([]);
  const [selectedParkingSpots, setSelectedParkingSpots] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isParticularDayModalVisible, setIsParticularDayModalVisible] =
    useState(false);
  const [parkingSpotModalData, setParkingSpotModalData] = useState({});
  const [reservationTime, setReservationTime] = useState(
    ReservationUtils.getFormattedTime({})
  );

  useEffect(() => {
    const fetchParkingSpots = async () =>
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parkingspots/${id}`,
        backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          if (data) setParkingSpots(data);
          setLoading(false);
        })
        .catch((err) => console.log(err));

    fetchParkingSpots();
  }, []);

  const showModal = (parkingSpot) => {
    setParkingSpotModalData(parkingSpot);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setIsParticularDayModalVisible(false);
    history.go(0);
  };

  const handleDayOk = (day) => {
    history.push("/home/particularDayReservation", {
      ids: selectedParkingSpots,
      parkingSpots: parkingSpots,
      day: day,
    });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleDayCancel = () => {
    setIsParticularDayModalVisible(false);
  };

  const handleDateAndTime = (time) => {
    setReservationTime(time);
    setLoading(true);
    const fetchParkingSpots = async () =>
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parkingspots`,
        backend("POST", {
          timeFrom: reservationTime.reservation_start,
          timeUntil: reservationTime.reservation_end,
          id: id,
        })
      )
        .then((res) => res.json())
        .then((data) => {
          if (data) setParkingSpots(data[0]);
          setLoading(false);
        })
        .catch((err) => console.log(err));
    fetchParkingSpots();
  };

  return (
    <div className={styles.container}>
      <List
        loading={loading}
        header={
          <div>
            <h1>Parking spots for {parkingSpots[0]?.parking?.name} </h1>
            <DateAndTimeForm handleDateAndTime={handleDateAndTime} />
          </div>
        }
        dataSource={parkingSpots}
        renderItem={(parkingSpot) => (
          <List.Item
            key={parkingSpot.id_parkingSpot}
            actions={[
              <Button
                size={"middle"}
                type={"secondary"}
                onClick={() => showModal(parkingSpot)}
                disabled={
                  !parkingSpot.reservation ||
                  parkingSpot.taken ||
                  user?.userRole !== "CLIENT"
                }
              >
                Reserve
              </Button>,
            ]}
          >
            <List.Item.Meta
              description={
                <Tag color={"purple"}>
                  Parking Spot {parkingSpot.parking.name} #
                  {parkingSpot.id_parkingSpot}
                </Tag>
              }
            />
            <Tag color={parkingSpot.reservation ? "green" : "red"}>
              {parkingSpot.reservation ? "Reservable" : "Not reservable"}
            </Tag>
          </List.Item>
        )}
      />
      {selectedParkingSpots.length > 0 && (
        <Button
          type={"primary"}
          onClick={() => setIsParticularDayModalVisible(true)}
        >
          Check selected parking spots availability
        </Button>
      )}
      {parkingSpots.length > 0 && (
        <Map
          selectedParkingSpots={selectedParkingSpots}
          setSelectedParkingSpots={setSelectedParkingSpots}
          center={
            parkingSpots.length > 0 &&
            MapCoordinatesUtils.getCenterOfPositionsArray(
              MapCoordinatesUtils.getAllSpotPositions(parkingSpots)
            )
          }
          polygons={MapCoordinatesUtils.getAllSpotPositions(parkingSpots)}
          styles={{
            boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
            height: window.innerHeight - 100,
            width: "50%",
            position: "absolute",
            top: "0px",
            right: "10px",
          }}
        />
      )}

      <ReservationModalConfirmation
        time={reservationTime}
        parkingSpot={parkingSpotModalData}
        visible={isModalVisible}
        handleOk={handleOk}
        handleCancel={handleCancel}
      />
      <ParticularDayFilterModal
        visible={isParticularDayModalVisible}
        handleOk={handleDayOk}
        handleCancel={handleDayCancel}
      />
    </div>
  );
};
