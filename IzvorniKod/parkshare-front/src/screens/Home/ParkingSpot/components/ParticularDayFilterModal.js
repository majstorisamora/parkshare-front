import { DatePicker, Modal } from "antd";
import { useState } from "react";
import { ReservationUtils } from "../../../../utils/ReservationUtils";

export const ParticularDayFilterModal = (props) => {
  const [day, setDay] = useState(new Date());

  const handleOk = () =>
    props.handleOk(ReservationUtils.getFormattedSingleDay(day));

  return (
    <Modal
      onOk={handleOk}
      onCancel={props.handleCancel}
      visible={props.visible}
      okText={"Check availability"}
    >
      <DatePicker onChange={(date) => setDay(date)} />
    </Modal>
  );
};
