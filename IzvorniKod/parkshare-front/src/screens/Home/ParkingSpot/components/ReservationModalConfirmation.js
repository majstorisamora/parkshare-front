import {
  Divider,
  Modal,
  notification,
  Radio,
  Spin,
  Statistic,
  Tag,
} from "antd";
import { useAuth } from "../../../../hooks/useAuth";
import styles from "./ReservationModalConfirmation.module.scss";
import { BsCreditCard } from "react-icons/bs";
import { useState } from "react";
import moment from "moment";
import { ReservationUtils } from "../../../../utils/ReservationUtils";
import { backend } from "../../../../utils/api";

export const ReservationModalConfirmation = (props) => {
  const { user } = useAuth();
  const [payStatus, setPayStatus] = useState("now");
  const [repetitive, setRepetitive] = useState(false);

  const handleOk = () => {
    notification.success(
      "Successfully reserved parking spot " + props.parkingSpot.id_parkingSpot
    );
    props.handleOk();
  };

  const handleReservationConfirmation = async () => {
    console.log(props.time);
    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/reservation/${props.parkingSpot.id_parkingSpot}`,
      backend("POST", {
        ...props.time,
        repetitive: repetitive,
        price:
          props?.parkingSpot?.parking?.price *
          ReservationUtils.getTotalReservationHours(props.time) *
          (repetitive ? 0.8 : 1),
      })
    ).catch((e) => console.log(e));

    res?.status === 200
      ? handleOk()
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          );
  };

  return (
    <Spin spinning={!props.parkingSpot}>
      <div className={styles.modal}>
        <Modal
          style={{ width: "80vw" }}
          title={
            <h3 style={{ marginBottom: "0" }}>
              Reservation for {props?.parkingSpot?.parking?.name} #
              {props.parkingSpot.id_parkingSpot}
            </h3>
          }
          visible={props.visible}
          onOk={handleReservationConfirmation}
          onCancel={props.handleCancel}
          okText={payStatus === "now" ? "Reserve & Pay" : "Reserve & Pay Later"}
        >
          <p>
            <b>Parking</b>: {props?.parkingSpot?.parking?.name}
          </p>
          <p>
            <b>Spot: </b> #{props?.parkingSpot?.id_parkingSpot}
          </p>
          <p>
            <b>Starting time: </b>{" "}
            {moment(props?.time.reservation_start).format("HH:mm, DD.MM.YY")}
            <b style={{ marginLeft: "10px" }}>End time: </b>
            {moment(props?.time.reservation_end).format("HH:mm, DD.MM.YY")}
          </p>
          <p>
            <b>User: </b>
            {user?.username}
          </p>
          <p>
            <b>Repetitive:&nbsp; </b>
            <Radio.Group
              onChange={(values) => setRepetitive(values.target.value)}
              defaultValue={false}
            >
              <Radio value={true}>True</Radio>
              <Radio value={false}>False</Radio>
            </Radio.Group>
          </p>
          <p>
            <b>Payment:&nbsp; </b>
            <Radio.Group
              onChange={(values) => setPayStatus(values.target.value)}
              defaultValue={"now"}
            >
              <Radio value={"now"}>Pay now</Radio>
            </Radio.Group>
          </p>
          {payStatus === "now" ? (
            <>
              <div className={styles.divider}>
                <Divider />
              </div>
              <p style={{ display: "flex", alignItems: "center" }}>
                <BsCreditCard size={24} />
                &nbsp;&nbsp;&nbsp;Card info&nbsp;&nbsp;&nbsp;
                <Tag color={user?.wallet_enabled ? "green" : "red"}>
                  {user?.wallet_enabled ? "Enabled" : "Disabled"}
                </Tag>
              </p>
              <p>
                <b>Card number: </b> {user?.numOfCreditCard}
              </p>
              <p>
                <b>Account balance : </b>
                {user?.wallet ? user.wallet : "0"}kn
              </p>
              <div className={styles.divider}>
                <Divider />
              </div>
            </>
          ) : (
            <></>
          )}

          <div className={styles.cost}>
            <Statistic
              title="Cost per hour"
              value={props?.parkingSpot?.parking?.price.toFixed(2)}
              suffix="kn/h"
            />
            <h1>x</h1>
            <Statistic
              title="Reservation hours"
              value={ReservationUtils.getTotalReservationHours(props.time)}
              suffix="h"
            />
            {repetitive && (
              <>
                <h1>-</h1>
                <Statistic title="Discount" value={20} suffix="%" />
              </>
            )}

            <h1>=</h1>
            <Statistic
              title="Total cost"
              value={(
                props?.parkingSpot?.parking?.price *
                ReservationUtils.getTotalReservationHours(props.time) *
                (repetitive ? 0.8 : 1)
              ).toFixed(2)}
              suffix="kn"
            />
          </div>
        </Modal>
      </div>
    </Spin>
  );
};
