import { useEffect, useState } from "react";
import { Avatar, Button, List, Select, Spin } from "antd";
import { ImageUtils } from "../../../utils/ImageUtils";
import styles from "./ParkingListAndMap.module.scss";
import { Map } from "../../../components/Map/Map";
import { MapCoordinatesUtils } from "../../../utils/MapCoordinatesUtils";
import { useHistory } from "react-router-dom";

const { Option } = Select;

export const ParkingListAndMapContainer = () => {
  const history = useHistory();
  const [filter, setFilter] = useState(["car", "bike"]);
  const [parkings, setParkings] = useState([]);

  useEffect(() => {
    const fetchParking = async () =>
      await fetch(`${process.env.REACT_APP_BACKEND_URL}/parking/all`, {
        method: "GET",
      })
        .then((res) => res.json())
        .then((data) => {
          setParkings(data);
        })
        .catch((err) => console.log(err));

    fetchParking();
  }, []);

  const handleMoreAboutParkingClick = (parking) => {
    history.push(`/home/parkingSpots/${parking.id_parking}`);
  };

  const handleSelectChange = (value) => {
    switch (value) {
      case "bike":
        setFilter(["bike"]);
        break;
      case "car":
        setFilter(["car"]);
        break;
      case "all":
        setFilter(["bike", "car"]);
        break;
    }
  };

  return (
    <div className={styles.container}>
      <Spin spinning={!parkings.length > 0}>
        <List
          header={
            <div>
              <h1>All parkings</h1>
              <div>
                Type:&nbsp;
                <Select onChange={handleSelectChange} defaultValue="all">
                  <Option value="all">All</Option>
                  <Option value="car">Car</Option>
                  <Option value="bike">Bike</Option>
                </Select>
              </div>
            </div>
          }
          dataSource={parkings.filter((parking) =>
            filter.includes(parking.type)
          )}
          renderItem={(parking) => (
            <List.Item
              key={parking.id_parking}
              actions={[
                parking.type === "car" ? (
                  <Button
                    type={"secondary"}
                    onClick={() => handleMoreAboutParkingClick(parking)}
                  >
                    See more
                  </Button>
                ) : (
                  <p>Spots: {parking.number_of_spots}</p>
                ),
              ]}
            >
              <List.Item.Meta
                avatar={<Avatar src={ImageUtils.getImageUrl(parking.image)} />}
                title={<a href="https://ant.design">{parking.name}</a>}
                description={
                  "Type: " +
                  parking.type.charAt(0).toUpperCase() +
                  parking.type.slice(1)
                }
              />
              {parking.description}
            </List.Item>
          )}
        />
      </Spin>

      <Map
        markers={MapCoordinatesUtils.getAllParkingsPostition(
          parkings.filter((parking) => filter.includes(parking.type))
        )}
        parkings={parkings}
        styles={{
          height: window.innerHeight - 100,
          width: "60%",
          position: "absolute",
          top: "0px",
          right: "10px",
        }}
      />
    </div>
  );
};
