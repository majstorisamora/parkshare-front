import { useEffect, useState } from "react";
import { backend } from "../../../utils/api";
import {
  CartesianGrid,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { notification, Spin } from "antd";
import moment from "moment";
import { useAuth } from "../../../hooks/useAuth";

export const PODashboardContainer = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user } = useAuth();

  useEffect(() => {
    const fetchStatistics = async () => {
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/statistics`,
        backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          data?.length > 0 &&
            setData(
              data.map((num, index) => {
                return {
                  num: num,
                  name: moment()
                    .subtract(1, "month")
                    .add(index, "month")
                    .startOf("month")
                    .format("MMMM"),
                };
              })
            );
          setLoading(false);
        })
        .catch((err) => {
          notification.error({ description: err.message });
          setLoading(false);
        });
    };
    if (user?.userRole === "PARKINGOWNER") fetchStatistics();
  }, []);

  return (
    <div>
      <Spin spinning={loading}>
        <h1>Reservations per Month</h1>
        <LineChart width={600} height={300} data={data}>
          <Line type="monotone" dataKey="num" stroke="#11453B" />
          <CartesianGrid stroke="#bbb" strokeDasharray="5 5" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
        </LineChart>
      </Spin>
    </div>
  );
};
