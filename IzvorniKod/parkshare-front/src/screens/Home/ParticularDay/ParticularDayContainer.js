import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { backend } from "../../../utils/api";
import styles from "./ParticularDay.module.scss";
import moment from "moment";
import { Button, List, notification, Tag } from "antd";
import { useAuth } from "../../../hooks/useAuth";

export const ParticularDayContainer = (props) => {
  const location = useLocation();
  const { login } = useAuth();
  const [takenAll, setTaken] = useState([]);

  useEffect(() => {
    const fetchAvailability = async () => {
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/reservation`,
        backend("POST", {
          ids: location.state.ids,
          date: location.state.day,
        })
      )
        .then((res) => res.json())
        .then((data) => {
          setTaken(data);
        })
        .catch((err) => console.log(err));
    };
    fetchAvailability();
  }, []);

  const renderParkingSpotAvailability = () => {
    return takenAll?.map((taken) => {
      return (
        <div className={styles.div}>
          <Tag color={"red"}>
            <p>
              {taken.parkingSpot.parking.name} #
              {taken.parkingSpot.id_parkingSpot} is&nbsp;taken between &nbsp;
              {moment(taken.reservation_start).format("HH:mm")}-
              {moment(taken.reservation_end).format("HH:mm")}
              &nbsp;
            </p>
          </Tag>
        </div>
      );
    });
  };

  const isNotTaken = (parkingSpot) => {
    const flag = takenAll?.find(
      (taken) => taken.parkingSpot.id_parkingSpot === parkingSpot.id_parkingSpot
    );

    if (flag) return false;
    return true;
  };

  const showModal = async (parkingSpot) => {
    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/reservation/${parkingSpot.id_parkingSpot}`,
      backend("POST", {
        reservation_start: moment(location.state.day).format(
          "YYYY-MM-DDTHH:mm:ss"
        ),
        reservation_end: moment(location.state.day)
          .add(24, "hours")
          .format("YYYY-MM-DDTHH:mm:ss"),
        repetitive: false,
        price: parkingSpot?.parking?.price * 24,
      })
    ).catch((err) => console.log(err));

    res?.status !== 200 &&
      res
        ?.json()
        .then((err) =>
          err?.message
            ? notification.error({ description: err.message })
            : notification.error({ description: err.status })
        );
  };

  return (
    <div>
      <h1>Availability for {location.state.day}</h1>
      <div style={{ display: "flex", flexDirection: "column" }}>
        {renderParkingSpotAvailability()}
        <List
          header={
            <div>
              <h1>Reserve for {location.state.day} </h1>
            </div>
          }
          dataSource={location.state.parkingSpots}
          renderItem={(parkingSpot) =>
            location.state.ids.includes(parkingSpot.id_parkingSpot) &&
            parkingSpot.reservation &&
            isNotTaken(parkingSpot) && (
              <List.Item
                key={parkingSpot.id_parkingSpot}
                actions={[
                  <Button
                    size={"middle"}
                    type={"secondary"}
                    onClick={() => showModal(parkingSpot)}
                  >
                    Reserve
                  </Button>,
                ]}
              >
                <List.Item.Meta
                  description={
                    <Tag color={"purple"}>
                      Parking Spot {parkingSpot.parking.name} #
                      {parkingSpot.id_parkingSpot}
                    </Tag>
                  }
                />
              </List.Item>
            )
          }
        />
      </div>
    </div>
  );
};
