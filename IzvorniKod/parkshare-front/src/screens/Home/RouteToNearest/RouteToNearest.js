import React, { useState } from "react";
import L from "leaflet";
import { MapContainer, Polyline, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-draw";
import { Button } from "antd";
import { useLocation } from "react-router-dom";

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-icon.png",
  iconUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-icon.png",
  shadowUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-shadow.png",
});

export const RouteToNearest = () => {
  const location = useLocation();
  const [payed, setPayed] = useState(false);

  const getPolyLine = () => {
    let helper = [];

    location.state.coordinates.coordinates.map((point) =>
      helper.push([point.x, point.y])
    );

    return <Polyline pathOptions={{ color: "lime" }} positions={helper} />;
  };

  return (
    <div style={{ display: "flex" }}>
      <MapContainer
        style={{ height: "50vh", width: "50vw" }}
        center={{ lat: 45.818459199925265, lng: 15.992422810177453 }}
        zoom={14}
      >
        {getPolyLine()}
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidmFsZW50aW52dWsiLCJhIjoiY2t0NjFrejg1MGU1cDJ2cGVwZmtvNGpwNyJ9.KvTXP86yDGRc4iu_PH9ajQ`}
        />
      </MapContainer>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <Button
          onClick={() => setPayed(true)}
          size={"big"}
          type={"primary"}
          style={{ marginLeft: "20px" }}
        >
          Arrived!
        </Button>
        {payed && <h1>Successfully payed for parking spot.</h1>}
      </div>
    </div>
  );
};
