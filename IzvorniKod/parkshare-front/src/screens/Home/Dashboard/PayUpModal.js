import { Form, Input, InputNumber } from "antd";
import Modal from "antd/es/modal/Modal";
import { useAuth } from "../../../hooks/useAuth";

export const PayUpModal = (props) => {
  const [formRef] = Form.useForm();
  const { user } = useAuth();

  const handleDepositMoney = () => {
    if (
      formRef.getFieldValue("numOfCreditCard").length == 16 &&
      formRef.getFieldValue("amount") > 0
    )
      props.depositInWallet(formRef.getFieldsValue());
  };

  return (
    <Modal
      title={<h3 style={{ marginBottom: "0" }}>Deposit money</h3>}
      onCancel={() => props.setIsVisible(false)}
      onOk={handleDepositMoney}
      okText={"Deposit"}
      visible={props.isVisible}
    >
      <Form form={formRef} name="basic" initialValues={user}>
        <Form.Item
          rules={[
            {
              required: true,
              message: "Please add credit card number.",
            },
            {
              validator: (rule, value) => {
                return value.length === 16
                  ? Promise.resolve()
                  : Promise.reject(new Error("Invalid credit card number"));
              },
            },
          ]}
          label="Credit Card Number"
          name="numOfCreditCard"
        >
          <Input />
        </Form.Item>
        <Form.Item
          rules={[
            {
              required: true,
              message: "Please insert amount to deposit.",
            },
          ]}
          label="Amount"
          name="amount"
        >
          <InputNumber min={0} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
