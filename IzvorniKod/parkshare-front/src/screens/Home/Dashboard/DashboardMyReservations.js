import { Card, Space } from "antd";
import moment from "moment";

export const DashboardMyReservations = (props) => {
  const renderMyReservations = () => {
    return props.myReservations.map((reservation) => {
      return (
        <Space key={reservation.id_reservation} align={"center"}>
          <p>
            <b>{reservation.parkingSpot.parking.name}</b>{" "}
            {moment(reservation.reservation_start).format("DD.MM")}
            &nbsp; &nbsp;
            {moment(reservation.reservation_start).format("HH:mm")}-
            {moment(reservation.reservation_end).format("HH:mm")}
            &nbsp; &nbsp;
          </p>
        </Space>
      );
    });
  };
  //todo: promijeniti u actually stvari
  return (
    <Card style={{ width: 400, marginTop: 16 }} title={"My reservations"}>
      {renderMyReservations()}
    </Card>
  );
};
