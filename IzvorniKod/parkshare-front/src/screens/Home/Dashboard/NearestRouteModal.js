import { Form, InputNumber, Select, Spin } from "antd";
import Modal from "antd/es/modal/Modal";
import { backend } from "../../../utils/api";
import { useHistory } from "react-router-dom";
import { useState } from "react";

const { Option } = Select;

export const NearestRouteModal = (props) => {
  const [formRef] = Form.useForm();
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  const handleGenerate = async () => {
    setLoading(true);
    await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/parkingspots/generateRouteToNearest`,
      backend("POST", {
        start: {
          x: props.location?.coords.latitude,
          y: props.location?.coords.longitude,
        },
        type: formRef.getFieldValue("type"),
        duration: formRef.getFieldValue("duration"),
      })
    )
      .then((data) => data.json())
      .then((res) => {
        history.push("/home/routeToNearest", {
          coordinates: res,
        });
      })
      .catch((err) => console.log(err));
  };

  return (
    <Modal
      title={<h3 style={{ marginBottom: "0" }}>Generate nearest route</h3>}
      onCancel={() => props.setIsVisible(false)}
      onOk={handleGenerate}
      okText={"Generate nearest route"}
      visible={props.isVisible}
    >
      <Spin spinning={loading}>
        <Form
          form={formRef}
          name="basic"
          initialValues={{ type: "car", duration: 1 }}
        >
          <Form.Item
            rules={[
              {
                required: true,
                message: "Please select type of vehicle.",
              },
            ]}
            label="Type"
            name="type"
          >
            <Select defaultValue="car">
              <Option value="car">Car</Option>
              <Option value="bike">Bike</Option>
            </Select>
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: "Please insert valid duration.",
              },
            ]}
            label="Duration"
            name="duration"
          >
            <InputNumber min={1} defaultValue={1} />
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};
