import { useAuth } from "../../../hooks/useAuth";
import { DashboardWallet } from "./DashboardWallet";
import { DashboardMyReservations } from "./DashboardMyReservations";
import { useEffect, useState } from "react";
import { backend } from "../../../utils/api";
import { PayUpModal } from "./PayUpModal";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import { NearestRouteModal } from "./NearestRouteModal";

export const DashboardContainer = () => {
  const { user, login } = useAuth();
  const history = useHistory();
  const [isVisible, setIsVisible] = useState(false);
  const [myReservations, setMyReservations] = useState([]);
  const [myLocation, setMyLocation] = useState({});
  const [isNearestVisible, setIsNearestVisible] = useState(false);

  useEffect(() => {
    const fetchReservations = async () => {
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/reservation/user`,
        backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          setMyReservations(data);
        })
        .catch((err) => console.log(err));

      await window.navigator.geolocation.getCurrentPosition((pos) => {
        setMyLocation(pos);
      });
    };

    fetchReservations();
  }, []);

  const depositInWallet = async (values) => {
    await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/user/wallet/deposit`,
      backend("PUT", values)
    )
      .then((res) => res.json())
      .then((data) => {
        setIsVisible(false);
        login(data).then(() => history.push("/home/dashboard"));
      })
      .catch((err) => console.log(err));
  };

  const activateWallet = async () => {
    await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/wallet/activate`, backend("GET"))
      .then((res) => res.json())
      .then((data) => {
        setIsVisible(false);
        login(data).then(() => history.push("/home/dashboard"));
      })
      .catch((err) => console.log(err));
  };

  const nearestRoute = async () => {
    setIsNearestVisible(true);
  };

  return (
    <div style={{ display: "flex", justifyContent: "space-around" }}>
      {user?.userRole === "CLIENT" && (
        <>
          <DashboardWallet
            setIsVisible={setIsVisible}
            activateWallet={activateWallet}
          />
          <DashboardMyReservations myReservations={myReservations} />
        </>
      )}
      <PayUpModal
        isVisible={isVisible}
        setIsVisible={setIsVisible}
        depositInWallet={depositInWallet}
      />
      <Button
        onClick={nearestRoute}
        size={"large"}
        type="primary"
        style={{ position: "absolute", bottom: "25px", right: "25px" }}
      >
        Generate closest route
      </Button>
      <NearestRouteModal
        location={myLocation}
        isVisible={isNearestVisible}
        setIsVisible={setIsNearestVisible}
      />
    </div>
  );
};
