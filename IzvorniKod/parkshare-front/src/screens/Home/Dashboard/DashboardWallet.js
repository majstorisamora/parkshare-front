import { Button, Card, Statistic } from "antd";
import { useAuth } from "../../../hooks/useAuth";

export const DashboardWallet = (props) => {
  const { user } = useAuth();

  return (
    <div>
      <Card
        title={"Wallet"}
        style={{ width: 300, marginTop: 16 }}
        actions={[
          <Button
            onClick={props.activateWallet}
            disabled={user?.wallet_enabled}
          >
            Activate
          </Button>,
          <Button type={"primary"} onClick={() => props.setIsVisible(true)}>
            Pay up
          </Button>,
        ]}
      >
        <Statistic
          title="Balance"
          value={user?.wallet ? user.wallet : 0}
          suffix="kn"
        />
      </Card>
    </div>
  );
};
