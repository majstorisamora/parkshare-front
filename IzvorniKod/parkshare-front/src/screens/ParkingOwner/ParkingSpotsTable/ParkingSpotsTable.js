import { useEffect, useState } from "react";
import { Button, notification, Spin, Table, Tag } from "antd";
import styles from "./ParkingSpotsTable.module.scss";
import { backend } from "../../../utils/api";

export const ParkingSpotsTable = () => {
  const [parkingSpots, setParkingSpots] = useState([]);
  const [parking, setParking] = useState({});

  useEffect(() => {
    const fetchParking = async () => {
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parking`,
        backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          setParking(data);
          return fetch(
            `${process.env.REACT_APP_BACKEND_URL}/parkingspots/${data.id_parking}`,
            backend("GET")
          );
        })
        .then((res) => res.json())
        .then((data) => {
          setParkingSpots(data);
        })
        .catch((err) => console.log(err));
    };

    fetchParking();
  }, []);

  const handleSeeSpotOnMap = () => {
    notification.info({ description: "We'll be done later." });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id_parkingSpot",
      key: "id_parkingSpot",
      render: (value) => {
        return <Tag color={"gold"}>{value}</Tag>;
      },
    },
    {
      title: "Reservation",
      dataIndex: "reservation",
      key: "reservation",
      render: (value) => {
        return value ? (
          <Tag color="green">Possible</Tag>
        ) : (
          <Tag color="red">Not possible</Tag>
        );
      },
    },
    {
      title: "Taken",
      dataIndex: "taken",
      key: "taken",
      render: (value) => {
        return value ? (
          <Tag color="red">Taken</Tag>
        ) : (
          <Tag color="green">Free</Tag>
        );
      },
    },
    {
      title: "See on the map",
      key: "seeMap",
      render: () => {
        return (
          <Button type={"default"} onClick={handleSeeSpotOnMap}>
            See on the map
          </Button>
        );
      },
    },
  ];

  return (
    <Spin spinning={!parkingSpots.length}>
      {parkingSpots.length > 0 && (
        <div className={styles.tableWrapper}>
          {parking ? <h1>{parking?.name}</h1> : <h1>Parking</h1>}
          <div className={styles.table}>
            <Table
              dataSource={parkingSpots}
              pagination={false}
              columns={columns}
              pagiat
              scroll={{ y: "500px" }}
            />
          </div>
        </div>
      )}
    </Spin>
  );
};
