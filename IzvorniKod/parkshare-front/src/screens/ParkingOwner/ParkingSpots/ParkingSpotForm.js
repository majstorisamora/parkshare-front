import { Button, Form, Switch } from "antd";

export const ParkingSpotForm = (props) => {
  const [formRef] = Form.useForm();

  return (
    <Form
      form={formRef}
      layout={"horizontal"}
      name="basic"
      onFinish={props.handleOnFinish}
    >
      <Form.Item label="Possible reservation" name="reservation">
        <Switch defaultChecked={false} />
      </Form.Item>
      <Button loading={props.loading} type={"primary"} htmlType={"submit"}>
        Add
      </Button>
    </Form>
  );
};
