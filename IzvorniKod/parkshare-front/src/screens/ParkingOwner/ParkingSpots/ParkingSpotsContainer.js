import { useEffect, useState } from "react";
import { notification } from "antd";
import styles from "./ParkingSpots.module.scss";
import { Map } from "../../../components/Map/Map";
import { ParkingSpotForm } from "./ParkingSpotForm";
import { backend } from "../../../utils/api";
import { MapCoordinatesUtils } from "../../../utils/MapCoordinatesUtils";
import { useAuth } from "../../../hooks/useAuth";
import { useHistory } from "react-router-dom";

export const ParkingSpotsContainer = () => {
  const [loading, setLoading] = useState(false);
  const [locations, setLocations] = useState([]);
  const [parking, setParking] = useState(false);
  const { user } = useAuth();
  const [parkingSpots, setParkingSpots] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const fetchParking = async () =>
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parking`,
        backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          setParking(data);
          return data;
        })
        .then(async (data) => {
          await fetch(
            `${process.env.REACT_APP_BACKEND_URL}/parkingspots/${data?.id_parking}`,
            backend("GET")
          )
            .then((res) => res.json())
            .then((data) => {
              if (data) setParkingSpots(data);
              setLoading(false);
            })
            .catch((err) => console.log(err));
        })
        .catch((err) => console.log(err));
    if (user?.idParking) {
      fetchParking();
    }
  }, [user]);

  const handleOnFinish = async (values) => {
    setLoading(true);
    if (locations.length === 0) {
      notification.error({ description: "Please add a spot on the map" });
      setLoading(false);
    } else {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parkingspots/${parking.id_parking}`,
        backend("POST", {
          reservation:
            values.reservation === undefined ? false : values.reservation,
          taken: false,
          location1: `${locations[0][0]?.lat};${locations[0][0]?.lng}`,
          location2: `${locations[0][1]?.lat};${locations[0][1]?.lng}`,
          location3: `${locations[0][2]?.lat};${locations[0][2]?.lng}`,
          location4: `${locations[0][3]?.lat};${locations[0][3]?.lng}`,
        })
      ).catch((err) => console.log(err));

      setLoading(false);
      res?.status === 200
        ? handleSuccess()
        : res
            ?.json()
            .then((err) =>
              err?.message
                ? notification.error({ description: err.message })
                : notification.error({ description: err.status })
            )
            .catch(() =>
              notification.error({ description: "Upali server dora." })
            );
    }
  };

  const handleSuccess = () => {
    notification.success({
      description: "Successfully added parking spot.",
      placement: "bottomRight",
    });
    history.go(0);
  };
  const handleSetLocations = (locations) => {
    setLocations(locations[Object.keys(locations)[0]]._latlngs);
  };

  return (
    <div className={styles.container}>
      {user?.idParking && parking?.type === "car" && (
        <Map
          onlyShow={true}
          polygons={
            parkingSpots
              ? MapCoordinatesUtils.getAllSpotPositions(parkingSpots)
              : false
          }
          center={
            parking?.location
              ? MapCoordinatesUtils.getPosition(parking.location)
              : false
          }
          handleSetLocations={handleSetLocations}
          location={
            parking?.location
              ? MapCoordinatesUtils.getPosition(parking.location)
              : false
          }
          parkingSpots={true}
        />
      )}
      <div className={styles.formContainer}>
        <h1>Add parking spot</h1>
        <ParkingSpotForm loading={loading} handleOnFinish={handleOnFinish} />
      </div>
    </div>
  );
};
