import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styles from "./Parking.module.scss";
import { Map } from "../../../components/Map/Map";
import { ParkingForm } from "./ParkingForm";
import { notification } from "antd";
import { backend } from "../../../utils/api";
import { MapCoordinatesUtils } from "../../../utils/MapCoordinatesUtils";

export const ParkingContainer = () => {
  const history = useHistory();
  const [parking, setParking] = useState(false);
  const [location, setLocation] = useState(false);

  useEffect(() => {
    const fetchParking = async () =>
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/parking`, backend("GET")
      )
        .then((res) => res.json())
        .then((data) => {
          if (data?.id_parking) setParking(data);
        })
        .catch((err) => console.log(err));

    fetchParking().then();
  }, []);

  const handleOnFinish = async (values, image) => {
    const test = location.lat + ";" + location.lng;
    const data = new FormData();
    data.append("name", values.name);
    data.append("type", values.type);
    data.append(
      "numberOfSpots",
      values.type === "bike" ? values.number_of_spots : 0
    );
    data.append("location", location ? test : parking["location"]);
    data.append("description", values.description);
    data.append("price", values.type === "bike" ? 0 : values.price);
    parking || data.append("image", image);

    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/parking`, {
      method: parking ? "PUT" : "POST",
      body: data,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("parkshareToken"),
      },
    }).catch((err) => console.log(err));

    res?.status === 200
      ? handleSuccess()
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          )
          .catch((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          );
  };

  const handleSuccess = () => {
    history.go(0);
  };

  const handleSetLocation = (location) => {
    setLocation(location[Object.keys(location)[0]]._latlng);
  };

  return (
    <div className={styles.container}>
      <Map
        center={
          parking?.location
            ? MapCoordinatesUtils.getPosition(parking.location)
            : false
        }
        location={
          parking?.location
            ? MapCoordinatesUtils.getPosition(parking.location)
            : false
        }
        handleSetLocations={handleSetLocation}
      />
      <div className={styles.formContainer}>
        <h1>
          {parking && (
            <img
              style={{ width: "100px", height: "100px" }}
              alt={"parking image"}
              src={`data:image/jpeg;base64,${parking.image}`}
            />
          )}
          {parking.name}
        </h1>
        <ParkingForm parking={parking} handleOnFinish={handleOnFinish} />
      </div>
    </div>
  );
};
