import { Button, Form, Input, InputNumber, Select } from "antd";
import { useEffect, useState } from "react";

const { TextArea } = Input;
const { Option } = Select;

export const ParkingForm = (props) => {
  const [formRef] = Form.useForm();
  const [picture, setPicture] = useState();
  const [status, setStatus] = useState();

  useEffect(() => {
    formRef.resetFields();
    setStatus(formRef.getFieldValue("type"));
  }, [props, formRef]);

  const handleOk = (values) => {
    props.handleOnFinish(values, picture);
  };

  const handleChange = (values) => {
    if (values?.type) setStatus(values.type);
  };

  return (
    <Form
      form={formRef}
      layout={"horizontal"}
      name="basic"
      onFinish={handleOk}
      initialValues={props?.parking}
      onValuesChange={handleChange}
    >
      <Form.Item
        rules={[
          {
            required: true,
            message: "Please input your name!",
          },
        ]}
        label="Name"
        name="name"
      >
        <Input />
      </Form.Item>
      <Form.Item
        rules={[
          {
            required: true,
            message: "Please select parking type!",
          },
        ]}
        label="Type"
        name="type"
        hidden={props?.parking}
        style={{ width: "260px" }}
      >
        <Select style={{ width: "260px" }}>
          <Option value="bike">Bike</Option>
          <Option value="car">Car</Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="Number of spots"
        name="number_of_spots"
        hidden={props?.parking}
      >
        <InputNumber defaultValue={0} min={0} />
      </Form.Item>
      {!props?.parking ? (
        <Form.Item
          rules={[
            {
              required: true,
              message: "Please upload parking image.",
            },
          ]}
          label="Parking image"
          name="imageUrl"
          hidden={props?.parking}
        >
          <input
            type={"file"}
            onChange={(d) => {
              setPicture(d.target.files[0]);
            }}
          />
        </Form.Item>
      ) : (
        <></>
      )}

      <Form.Item
        rules={[
          {
            required: true,
            message: "Please describe your parking!",
          },
        ]}
        label={"Description"}
        name={"description"}
      >
        <TextArea rows={3} />
      </Form.Item>
      {status === "car" ? (
        <Form.Item
          rules={[
            {
              required: true,
              message: "Please select parking price!",
            },
          ]}
          label="Price(hour)"
          name="price"
        >
          <InputNumber
            defaultValue={0}
            min={0}
            step={0.5}
            precision={2}
            formatter={(value) =>
              `${value}kn`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }
            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
          />
        </Form.Item>
      ) : (
        <></>
      )}

      <Form.Item>
        <Button type={"primary"} htmlType={"submit"}>
          {props.parking ? "Update" : "Create"}
        </Button>
      </Form.Item>
    </Form>
  );
};
