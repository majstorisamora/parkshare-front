import { Button, Form, Input, Modal } from "antd";
import { useHistory } from "react-router-dom";
import styles from "./LogIn.module.scss";
import { useState } from "react";

export const LoginForm = (props) => {
  const history = useHistory();
  const [visible, setVisible] = useState(false);
  const [formRef] = Form.useForm();

  const handleOnFinish = (values) => {
    props.handleLogin(values);
  };
  const handleSignUpRedirect = () => {
    history.push("/signup");
  };
  const handleRequestNewPassword = (values) => {
    props.handleRequestNewPassword(values);
  };

  return (
    <>
      <Form
        form={formRef}
        name="basic"
        onFinish={handleOnFinish}
        className={styles.form}
        data-testid="formLoginTest"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input data-testid="formInputUsername" />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password data-testid="formInputPassword" />
        </Form.Item>
        <div className={styles.div}>
          <Form.Item>
            <Button type="link" onClick={handleSignUpRedirect}>
              Sign Up
            </Button>
          </Form.Item>

          <Form.Item>
            <Button
              data-testid="submitBtn"
              loading={props.loading}
              type="primary"
              htmlType="submit"
            >
              Submit
            </Button>
          </Form.Item>
        </div>
        <div className={styles.password}>
          <Form.Item>
            <Button
              data-testid="generatePassword"
              type="secondary"
              onClick={() => setVisible(true)}
            >
              Generate New Password
            </Button>
          </Form.Item>
        </div>
      </Form>
      <Modal
        title={"Generate New Password"}
        visible={visible}
        onOk={handleRequestNewPassword}
        onCancel={() => setVisible(false)}
        footer={null}
        data-testid="modalCheck"
      >
        <Form onFinish={handleRequestNewPassword}>
          <Form.Item
            label="E-mail"
            name="email"
            validateFirst={true}
            rules={[
              {
                required: true,
                message: "Please input your email address!",
              },
              {
                validator: (rule, value) => {
                  return value.includes("@")
                    ? Promise.resolve()
                    : Promise.reject(new Error("Invalid email"));
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className={styles.div}>
            <Form.Item>
              <Button type="secondary" onClick={() => setVisible(false)}>
                Cancel
              </Button>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
};
