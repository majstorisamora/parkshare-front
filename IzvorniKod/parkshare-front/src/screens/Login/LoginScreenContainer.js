import { LoginForm } from "./LoginForm";
import { useHistory } from "react-router-dom";
import { Layout, notification } from "antd";
import { useEffect, useState } from "react";
import styles from "./LogIn.module.scss";
import { Header } from "../../components/Header/Header";
import { backend, getAuthenticate } from "../../utils/api";
import { useAuth } from "../../hooks/useAuth";

export const LoginScreenContainer = () => {
  const { user, login } = useAuth();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  useEffect(() => {
    user && history.push("/home");
  }, []);

  const handleLogin = async (values) => {
    setLoading(true);

    const token = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/authenticate`,
      getAuthenticate("POST", values)
    ).catch((err) => console.log(err));

    await token
      .json()
      .then((data) => localStorage.setItem("parkshareToken", data.jwt));

    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/login`,
      backend("POST", values)
    ).catch((err) => console.log(err));

    setLoading(false);
    res?.status === 200
      ? handleSuccess(res)
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({
                  description: "User doesn't exist in database.",
                })
          )
          .catch(() =>
            notification.error({ description: "Upali server dora." })
          );
  };

  const handleSuccess = (res) => {
    res.json().then((data) =>
      login(data).then(() => {
        history.push("/home/parking/all");
        notification.success({
          description: "Successfully logged in.",
          placement: "bottomRight",
        });
      })
    );
  };

  const handleRequestNewPassword = async (values) => {
    if (values) {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/login/forgotPassword`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(values),
        }
      ).catch((err) => console.log(err));

      res?.status === 200
        ? notification.success({
            description: "Generated new password. Check your email.",
          })
        : res
            ?.json()
            .then((err) =>
              err?.message
                ? notification.error({ description: err.message })
                : notification.error({
                    description: "User doesn't exist in database.",
                  })
            )
            .catch(() =>
              notification.error({ description: "Upali server dora." })
            );
    } else {
      notification.error({
        description: "Please fill username to generate new password.",
      });
    }
  };

  return (
    <div className={styles.layout}>
      <Layout>
        <Header />
        <LoginForm
          loading={loading}
          handleLogin={handleLogin}
          handleRequestNewPassword={handleRequestNewPassword}
        />
      </Layout>
    </div>
  );
};
