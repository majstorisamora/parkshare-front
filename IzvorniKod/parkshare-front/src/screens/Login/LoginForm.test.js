import { fireEvent, getByTestId, render, screen } from "@testing-library/react";
import { LoginForm } from "./LoginForm";

describe("Testing Login Form Render", () => {
  window.matchMedia =
    window.matchMedia ||
    function () {
      return {
        matches: false,
        addListener: function () {},
        removeListener: function () {},
      };
    };

  it("check for elements in form are rendering correctly", () => {
    const { getByTestId } = render(<LoginForm />);

    // Get elements to be checked if they render correctly
    const form = getByTestId("formLoginTest");
    const usernameInput = getByTestId("formInputUsername");
    const usernamePassword = getByTestId("formInputPassword");
    const submitButton = getByTestId("submitBtn");

    // Check elements to be true
    expect(form).toBeTruthy();
    expect(usernameInput).toBeTruthy();
    expect(usernamePassword).toBeTruthy();
    expect(submitButton).toBeTruthy();
  });

  it("check if modal is working", async () => {
    const { getByTestId } = render(<LoginForm />);
    // Modal should not be active
    const modalBeforeClick = screen.queryAllByTestId("modalCheck");
    expect(modalBeforeClick).toHaveLength(0);
    // Click on button activate modal
    const btnGenerate = getByTestId("generatePassword");
    fireEvent.click(btnGenerate);
    // Modal Should be active
    const modalAfterClick = getByTestId("modalCheck");
    expect(modalAfterClick).toBeTruthy();
  });
});
