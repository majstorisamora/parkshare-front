import { Button, Form, Input, Select } from "antd";
import { useEffect } from "react";
import { useAuth } from "../../hooks/useAuth";

const { TextArea } = Input;
const { Option } = Select;

export const UserSettingsModal = (props) => {
  const [formRef] = Form.useForm();

  useEffect(() => {
    formRef.resetFields();
  });
  console.log(props.user);
  const renderCardStatus = () => {

    if (props.user.wallet_enabled && props.user.numOfCreditCard && props.user.userRole=='CLIENT') {
      return <Form.Item label="Credit Card Number"name="numOfCreditCard">
        <Input />
      </Form.Item>
    }else if (props.user.wallet_enabled && !props.user.numOfCreditCard && props.user.userRole=='CLIENT') {
      return <div className="cardNotActive">CARD IS ACTIVATED WITHOUT CARD NUMBER</div>
    }else if(!props.user.wallet_enabled && !props.user.numOfCreditCard && props.user.userRole=='CLIENT') {
      return <div className="cardNotActive">CARD IS NOT ACTIVATED</div>
    }

  }

  return (
      <>
        <div className="updateForm">
          <Form
              form={formRef}
              layout={"horizontal"}
              name="basic"
              onFinish={props.handleOnFinish}
              initialValues={props.user}
          >
            <Form.Item label="Email" name="email">
              <Input />
            </Form.Item>
            <Form.Item label="First name" name="firstName" >
              <Input/>
            </Form.Item>
            <Form.Item
                label="IBAN"
                name="iban"
            >
              <Input/>
            </Form.Item>
            <Form.Item label="Last name" name="lastName">
              <Input />
            </Form.Item>
            <Form.Item label="Username"name="username">
              <Input />
            </Form.Item>

            {renderCardStatus()}

            <Form.Item>
              <Button type={"primary"} htmlType={"submit"}>
                {props.user ? "Update" : "Create"}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </>
  );
};