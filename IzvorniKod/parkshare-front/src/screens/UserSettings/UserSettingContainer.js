import { useEffect, useState } from "react";
import { notification } from "antd";
import { UserSettingsModal } from "./UserSettings.Modal";
import { useHistory } from "react-router-dom";
import { backend } from "../../utils/api";
import { useAuth } from "../../hooks/useAuth";

export const UserSettingsContainer = (props) => {
    const history = useHistory();
    const { user: authUser } = useAuth();
    const [user, setUser] = useState([]);
    const [editUser, setEditUser] = useState({});

    useEffect(() => {
        const fetchUser = async () =>
            await fetch(`${process.env.REACT_APP_BACKEND_URL}/user`, backend("GET"))
                .then((res) => res.json())
                .then((data) => {
                    setUser(data);
                })
                .catch((err) => console.log(err));

        fetchUser();
    }, []);
    const handleSuccess = () => {
        notification.success({
            description: "Successfully updated.",
            placement: "topRight",
        });
    };

    const handleOnFinish = async (values) => {
        console.log(values);
        const res = await fetch(
            `${process.env.REACT_APP_BACKEND_URL}/user`,
            //todo: provjeriti ovo
            backend("PUT", {
                email: values.email,
                firstName: values.firstName,
                iban: values.iban,
                lastName: values.lastName,
                username: values.username,
                numOfCreditCard: values.numOfCreditCard,
                wallet_enabled: values.wallet_enabled,
                userRole: values.userRole
            })
        ).catch((err) => console.log(err));
        res?.status === 200
            ? handleSuccess()
            : res
                ?.json()
                .then((err) =>
                    err?.message
                        ? notification.error({ description: err.message })
                        : notification.error({ description: err.status })
                )
                .catch(() =>
                    notification.error({ description: "Upali server dora." })
                );
    };
    return (
        <>
            <img
                alt={"profile pic"}
                style={{ width: "100px" }}
                src={`data:image/jpeg;base64,${user.imageUrl}`}
            />
            <UserSettingsModal
                editUser={editUser}
                handleOnFinish={handleOnFinish}
                user={user}
            />
        </>
    );
};