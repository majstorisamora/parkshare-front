import { useEffect, useState } from "react";
import { Button, notification, Spin, Table, Tag } from "antd";
import { useHistory } from "react-router-dom";

export const AdminParkingOwnersContainer = () => {
  const history = useHistory();
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () =>
      await fetch(`${process.env.REACT_APP_BACKEND_URL}/users`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Basic " + window.btoa("admin:pass"),
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUsers(data);
        })
        .catch((err) => console.log(err));

    fetchUsers();
  }, []);

  const activateUser = async (record) => {
    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/users/confirmOwner/${record.username}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Basic " + window.btoa("admin:pass"),
        },
      }
    )
      .then(() =>
        notification.success({
          description: "Successfully activated parking owner.",
        })
      )
      .catch((err) => console.log(err));
    history.go(0);
  };

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Status",
      dataIndex: "enabled",
      key: "enabled",
      render: (value) => {
        if (value === 0)
          return <Tag color={"default"}>EMAIL NOT CONFIRMED</Tag>;
        return <Tag color={"yellow"}>WAITING</Tag>;
      },
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => {
        return (
          <Button
            disabled={record.enabled === 0}
            onClick={() => activateUser(record)}
          >
            Activate
          </Button>
        );
      },
    },
  ];

  return (
    <Spin spinning={!users.length}>
      {users.length && (
        <Table
          dataSource={users
            .filter(
              (user) =>
                user.userRole === "PARKINGOWNER" &&
                (user.enabled === 1 || user.enabled === 0)
            )
            .map((user) => {
              return { ...user, key: user.id };
            })}
          columns={columns}
        />
      )}
    </Spin>
  );
};
