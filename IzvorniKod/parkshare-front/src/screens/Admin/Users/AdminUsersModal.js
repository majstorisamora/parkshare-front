import { Modal, Spin } from "antd";
import { SignUpForm } from "../../Signup/SignUpForm";

export const AdminUsersModal = (props) => {
  const handleSubmit = (values) => props.handleEditUser(values);
  return (
    <Spin spinning={!props.editUser}>
      <Modal
        title={props.editUser.username}
        visible={props.visible}
        onOk={props.handleOk}
        onCancel={props.handleCancel}
        footer={null}
      >
        <SignUpForm
          editUser={props.editUser}
          showDivider={false}
          handleSignUp={handleSubmit}
        />
      </Modal>
    </Spin>
  );
};
