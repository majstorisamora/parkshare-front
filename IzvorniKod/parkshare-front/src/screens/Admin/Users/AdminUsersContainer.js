import { useEffect, useState } from "react";
import {
  Button,
  notification,
  Popconfirm,
  Space,
  Spin,
  Table,
  Tag,
} from "antd";
import { AdminUsersModal } from "./AdminUsersModal";
import { useHistory } from "react-router-dom";
import styles from "./AdminUsers.module.scss";

export const AdminUsersContainer = () => {
  const history = useHistory();
  const [users, setUsers] = useState([]);
  const [editUser, setEditUser] = useState({});
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    const fetchUsers = async () =>
      await fetch(`${process.env.REACT_APP_BACKEND_URL}/users`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Basic " + window.btoa("admin:pass"),
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUsers(data);
        })
        .catch((err) => console.log(err));

    fetchUsers();
  }, []);

  const showModal = (index) => {
    setEditUser(users[index]);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    history.go(0);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleDeleteUser = async (record) => {
    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/users/${record.id}`,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Basic " + window.btoa("admin:pass"),
        },
      }
    );
    res?.status === 200
      ? handleOk()
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          );
  };

  const handleEditUser = async (values) => {
    const res = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/users/${editUser.id}`,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Basic " + window.btoa("admin:pass"),
        },
        body: JSON.stringify({
          id: editUser.id,
          enabled: editUser.enabled,
          wallet_enabled: editUser["wallet_enabled"],
          wallet: editUser.wallet,
          id_parking: editUser["id_parking"],
          username: values.username,
          firstName: values.firstName,
          lastName: values.lastName,
          imageUrl: values.imageUrl,
          iban: values.iban,
          email: values.email,
          password: editUser.password,
          userRole: editUser.userRole,
        }),
      }
    ).catch((err) => console.log(err));

    res?.status === 200
      ? handleOk()
      : res
          ?.json()
          .then((err) =>
            err?.message
              ? notification.error({ description: err.message })
              : notification.error({ description: err.status })
          );
  };

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "IBAN",
      dataIndex: "iban",
      key: "iban",
    },
    {
      title: "User Role",
      dataIndex: "userRole",
      key: "userRole",
      render: (value) => {
        if (value === "CLIENT") return <Tag color={"orange"}>CLIENT</Tag>;
        if (value === "PARKINGOWNER")
          return <Tag color={"green"}>PARKING OWNER</Tag>;
        return <Tag color={"red"}>ADMIN</Tag>;
      },
    },
    {
      title: "Action",
      key: "action",
      render: (text, record, index) => {
        return (
          <Space size="middle" className={styles.popover}>
            <Button
              disabled={record.userRole === "ADMIN"}
              onClick={() => showModal(index)}
            >
              Edit
            </Button>
            {record.userRole === "ADMIN" ? (
              <Button disabled>Delete</Button>
            ) : (
              <Popconfirm
                title={"Are you sure?"}
                onConfirm={() => handleDeleteUser(record)}
                okText={"Yes"}
                cancelText={"No"}
              >
                <Button type={"primary"} danger>
                  Delete
                </Button>
              </Popconfirm>
            )}
          </Space>
        );
      },
    },
  ];

  return (
    <Spin spinning={!users.length}>
      {users.length > 0 && (
        <>
          <Table
            dataSource={users.map((user) => {
              return { ...user, key: user.id };
            })}
            columns={columns}
          />
          <AdminUsersModal
            editUser={editUser}
            visible={isModalVisible}
            handleEditUser={handleEditUser}
            handleOk={handleOk}
            handleCancel={handleCancel}
          />
        </>
      )}
    </Spin>
  );
};
