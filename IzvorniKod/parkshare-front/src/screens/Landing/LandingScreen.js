import { Button, Layout } from "antd";
import { useHistory } from "react-router-dom";
import styles from "./LandingScreen.module.scss";

const { Content, Footer } = Layout;

export const LandingScreen = () => {
  const history = useHistory();

  return (
    <Layout style={{ height: "100vh" }}>
      <Content className={styles.content}>
        <h1>ParkShare</h1>
        <section>
          <Button type={"link"} onClick={() => history.push("/login")}>
            Log In
          </Button>
          <Button type={"link"} onClick={() => history.push("/parking/all")}>
            Parkings
          </Button>
          <Button type={"link"} onClick={() => history.push("/signup")}>
            Sign Up
          </Button>
        </section>
      </Content>
      <Footer className={styles.footer}>
        Made with ❤️ &nbsp;by MajstoriSaMora
      </Footer>
    </Layout>
  );
};
