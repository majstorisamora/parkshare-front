export class LocationUtils {
  static getUserLocation = async () =>
    await window.navigator.geolocation.getCurrentPosition((pos) => {
      return pos;
    });
}
