export class ImageUtils {
  static getImageUrl(url) {
    if (url) {
      return url.includes("http") ? url : "https://i.imgur.com/qqzSO.jpeg";
    }
    return "https://i.imgur.com/qqzSO.jpeg";
  }
}
