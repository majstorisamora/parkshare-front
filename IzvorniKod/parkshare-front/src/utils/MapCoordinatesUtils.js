export class MapCoordinatesUtils {
  static getPosition = (values) => {
    let helper = values.split(";");
    return { lat: parseFloat(helper[0]), lng: parseFloat(helper[1]) };
  };

  static getPositionInArray = (values) => {
    let helper = values.split(";");
    return [parseFloat(helper[0]), parseFloat(helper[1])];
  };

  static getAllParkingsPostition = (parkings) => {
    let helper = [];

    parkings.map((parking) => {
      helper.push(MapCoordinatesUtils.getPosition(parking.location));
    });
    return helper;
  };

  static getAllSpotPositions = (spots) => {
    let helper = [];

    spots.map((spot) => {
      helper.push({
        ...spot,
        location: [
          MapCoordinatesUtils.getPositionInArray(spot.location1),
          MapCoordinatesUtils.getPositionInArray(spot.location2),
          MapCoordinatesUtils.getPositionInArray(spot.location3),
          MapCoordinatesUtils.getPositionInArray(spot.location4),
        ],
      });
    });

    return helper;
  };

  static getCenterOfPositionsArray = (positions) => {
    if (Array.isArray(positions[0]?.location)) {
      return {
        lat: positions[0].location[0][0],
        lng: positions[0].location[0][1],
      };
    }
    return positions[0];
  };
}
