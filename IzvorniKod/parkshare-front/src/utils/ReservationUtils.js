import moment from "moment";

export class ReservationUtils {
  static getFormattedTime(allValues) {
    console.log(allValues);
    const reservationFromDate = allValues?.date
      ? allValues.date[0].format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD");
    const reservationFromHours = allValues?.time
      ? allValues.time[0].format("HH:mm:ss")
      : moment().format("HH:mm:ss");

    const reservationUntilDate = allValues?.date
      ? allValues.date[1].format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD");
    const reservationUntilHours = allValues?.time
      ? allValues.time[1].format("HH:mm:ss")
      : moment().add(1, "hours").format("HH:mm:ss");

    return {
      reservation_start: reservationFromDate + "T" + reservationFromHours,
      reservation_end: reservationUntilDate + "T" + reservationUntilHours,
    };
  }

  static getFormattedSingleDay(date) {
    return moment(date).format("YYYY-MM-DD");
  }

  static getTotalReservationHours(time) {
    return Math.ceil(
      moment(time.reservation_end).diff(time.reservation_start, "minutes") / 60
    );
  }
}
