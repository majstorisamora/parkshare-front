export const getAuthenticate = (method, values) => {
  return {
    method: method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(values),
  };
};

export const backend = (method, values) => {
  return {
    method: method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("parkshareToken"),
    },
    body: JSON.stringify(values),
  };
};
