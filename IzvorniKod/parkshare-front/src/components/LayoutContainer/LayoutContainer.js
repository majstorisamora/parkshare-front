import { Layout } from "antd";
import { Header } from "../Header/Header";
import styles from "./LayoutContainer.module.scss";
import { Sider } from "../Sider/Sider";
import { useAuth } from "../../hooks/useAuth";

const { Content } = Layout;

export const LayoutContainer = ({ children }) => {
  const { user } = useAuth();
  return (
    <Layout className={styles.layout} style={{ minHeight: "100vh" }}>
      {user && <Sider />}
      <Layout>
        <Header />
        <Content
          style={{ margin: "16px 16px", maxHeight: "calc(100vh - 80px)" }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};
