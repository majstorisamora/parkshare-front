import { DatePicker, Form, TimePicker } from "antd";
import styles from "./DateAndTimeForm.module.scss";
import moment from "moment";
import { ReservationUtils } from "../../utils/ReservationUtils";

export const DateAndTimeForm = (props) => {
  const handleFormFinish = (values, allValues) => {
    const reservationTime = ReservationUtils.getFormattedTime(allValues);
    props.handleDateAndTime(reservationTime);
  };

  return (
    <div className={styles.pickers}>
      <Form onValuesChange={handleFormFinish} layout={"inline"}>
        <Form.Item
          rules={[
            {
              required: true,
              message: "Please input date range!",
            },
          ]}
          validateFirst={true}
          name={"date"}
        >
          <DatePicker.RangePicker defaultValue={[moment(), moment()]} />
        </Form.Item>
        <Form.Item required={true} name={"time"}>
          <TimePicker.RangePicker
            defaultValue={[moment(), moment().add(1, "hours")]}
            format={"HH:mm"}
          />
        </Form.Item>
      </Form>
    </div>
  );
};
