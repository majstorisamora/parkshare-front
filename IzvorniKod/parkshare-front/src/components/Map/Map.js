import React, { useEffect, useRef, useState } from "react";
import L from "leaflet";
import {
  FeatureGroup,
  MapContainer,
  Marker,
  Polygon,
  TileLayer,
  Tooltip,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { EditControl } from "react-leaflet-draw";
import "leaflet-draw";

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-icon.png",
  iconUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-icon.png",
  shadowUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/marker-shadow.png",
});

export const Map = (props) => {
  const editableFG = useRef(null);
  const [map, setMap] = useState(null);
  const [ready, setReady] = useState(false);

  useEffect(() => {
    if (map) {
      map.setView(props?.center, 16);
    }
  }, [props?.center]);

  useEffect(() => {
    if (map && props?.polygons.length > 0) {
      setReady(true);
    }
  }, [props?.polygons]);

  const handleMapCreated = (map) => {
    setMap(map);
  };

  const onCreated = () => {
    const drawnItems = editableFG.current._layers;
    if (Object.keys(drawnItems).length > 1) {
      Object.keys(drawnItems).forEach((layerid, index) => {
        if (index > 0) return;
        const layer = drawnItems[layerid];
        editableFG.current.removeLayer(layer);
      });
    }
    props.handleSetLocations(drawnItems);
  };

  const renderAllMarkers = () => {
    if (props?.markers) {
      return props.markers.map((location, index) => (
        <Marker position={location}>
          <Tooltip>{props.parkings[index].name}</Tooltip>
        </Marker>
      ));
    }
  };
  const renderAllLocations = () => {
    return props.polygons.map((spot) => {
      return (
        <Polygon
          positions={spot.location}
          interactive={true}
          pathOptions={getPathColor(spot)}
          eventHandlers={
            props.onlyShow
              ? {}
              : {
                  click: () => {
                    props.setSelectedParkingSpots((prev) =>
                      props.selectedParkingSpots.includes(spot.id_parkingSpot)
                        ? prev.filter((a) => a !== spot.id_parkingSpot)
                        : [...prev, spot.id_parkingSpot]
                    );
                  },
                }
          }
        >
          <Tooltip>{spot.id_parkingSpot}</Tooltip>
        </Polygon>
      );
    });
  };

  const getPathColor = (spot) => {
    if (props.onlyShow) return { color: "green" };
    if (props.selectedParkingSpots.includes(spot?.id_parkingSpot)) {
      return { color: "purple" };
    }
    return { color: spot.reservation ? "green" : "red" };
  };

  return (
    <MapContainer
      whenCreated={handleMapCreated}
      center={{ lat: 45.818459199925265, lng: 15.992422810177453 }}
      zoom={props?.polygons ? 18 : 13}
      style={
        props?.styles
          ? props.styles
          : {
              height: window.innerHeight - 100,
              width: props?.parkingSpots ? "80vw" : "55vw",
            }
      }
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidmFsZW50aW52dWsiLCJhIjoiY2t0NjFrejg1MGU1cDJ2cGVwZmtvNGpwNyJ9.KvTXP86yDGRc4iu_PH9ajQ`}
      />
      {props?.markers ? (
        renderAllMarkers()
      ) : (
        <Marker
          position={props?.location ? props.location : { lat: 1.21, lng: 21.2 }}
        />
      )}

      {ready && props?.polygons && renderAllLocations()}

      <FeatureGroup ref={editableFG}>
        <EditControl
          draw={{
            rectangle: false,
            polygon: props?.parkingSpots,
            polyline: false,
            circlemarker: false,
            marker: !props?.parkingSpots,
            circle: false,
          }}
          position="topright"
          onCreated={onCreated}
        />
      </FeatureGroup>
    </MapContainer>
  );
};
