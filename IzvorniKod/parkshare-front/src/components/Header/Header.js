import styles from "./header.module.scss";
import { BiLogOut } from "react-icons/bi";
import { useHistory } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";

export const Header = () => {
  const { location } = useHistory();
  const { user, logout } = useAuth();

  const handleLogOut = () => {
    logout();
  };

  return (
    <header className={styles.header}>
      <p>ParkShare</p>
      {location.pathname !== "/" && user && (
        <BiLogOut
          color={"0D332C"}
          onClick={handleLogOut}
          size={28}
          className={styles.svg}
        />
      )}
    </header>
  );
};
