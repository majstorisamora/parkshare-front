import { Divider, Layout, Menu } from "antd";
import {
  RiAccountBoxLine,
  RiHome2Fill,
  RiListCheck2,
  RiSettings3Line,
} from "react-icons/ri";
import { useState } from "react";
import { Link } from "react-router-dom";
import styles from "./Sider.module.scss";
import { CgUserList } from "react-icons/cg";
import { MdCarRental } from "react-icons/md";
import { AiFillDatabase, AiOutlinePlusCircle } from "react-icons/ai";
import { useAuth } from "../../hooks/useAuth";

const { Sider: AntSider } = Layout;

export const Sider = () => {
  const [collapsed, setCollapsed] = useState(false);
  const { user } = useAuth();

  const renderAdminPanel = () => {
    return (
      <>
        <Divider style={{ color: "white" }}> Admin </Divider>
        <Menu.Item key="4" icon={<CgUserList size={24} />}>
          <Link to={"/admin/users"}>Users</Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<RiAccountBoxLine size={24} />}>
          <Link to={"/admin/activateOwners"}>Activate Owners</Link>
        </Menu.Item>
      </>
    );
  };

  const renderParkingOwnerPanel = () => {
    return (
      <>
        <Divider style={{ color: "white" }}> Owner </Divider>
        <Menu.Item key="4" icon={<MdCarRental size={24} />}>
          <Link to={"/parkingOwner/myParking"}>My parking</Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<AiFillDatabase size={24} />}>
          <Link to={"/parkingOwner/myParkingSpots"}>My parking spots</Link>
        </Menu.Item>
        <Menu.Item key="6" icon={<AiOutlinePlusCircle size={24} />}>
          <Link to={"/parkingOwner/addParkingSpot"}>Add parking spot</Link>
        </Menu.Item>
      </>
    );
  };

  return (
    <AntSider
      className={styles.sider}
      collapsible
      collapsed={collapsed}
      onCollapse={() => setCollapsed((prev) => !prev)}
    >
      <Menu theme={"dark"}>
        <Divider style={{ color: "white" }}> General </Divider>
        <Menu.Item key="1" icon={<RiHome2Fill size={24} />}>
          <Link to={"/home/dashboard"}>Dashboard</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<RiListCheck2 size={24} />}>
          <Link to={"/home/parking/all"}>Parkings</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<RiSettings3Line size={24} />}>
          <Link to={"/home/settings"}>Settings</Link>
        </Menu.Item>

        {user?.userRole === "ADMIN" && renderAdminPanel()}
        {user?.userRole === "PARKINGOWNER" && renderParkingOwnerPanel()}
      </Menu>
    </AntSider>
  );
};
