import { createContext, useContext, useEffect, useState } from "react";

const authContext = createContext();

function useAuthentication() {
  const [user, setUser] = useState(undefined);

  useEffect(() => {
    const localState = localStorage.getItem("parkshareState");

    if (localState) {
      setUser(JSON.parse(localState));
    }
  }, []);

  return {
    user,
    login(loginUserData) {
      return new Promise((res) => {
        setUser(loginUserData);
        localStorage.setItem("parkshareState", JSON.stringify(loginUserData));
        res();
      });
    },
    logout() {
      return new Promise((res) => {
        setUser(undefined);
        localStorage.removeItem("parkshareState");
        res();
      });
    },
  };
}

export function AuthProvider({ children }) {
  const auth = useAuthentication();

  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};
